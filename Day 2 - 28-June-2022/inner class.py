class outer:
  def __init__(self):
    self.name = "john"
    self.age = 45 
    self.inner = self.inner()

  def show(self):
    print(self.name, self.age)

  class inner:
    def __init__(self):
      self.age = 45

    def show(self):
      print(self.age)

obj = outer()

obj.show()

lap = outer.inner()

lap.show()