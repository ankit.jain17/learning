def div(a,b):
  return print(a/b)

def deco(fun):

  def inner(a,b):
    if a<b:
      a,b=b,a
    return fun(a,b) #updated values
  return inner

div = deco(div)

div(2,4)

