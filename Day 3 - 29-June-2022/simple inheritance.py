class a:# parent class

  def fun_1(self):
   return print("Function 1")

  def fun_2():
    return print("Function 2")

class b(a):# child class
  def fun_3():
    return print("Function 3")

  def fun_4():
    return print("Function 4")

class c(b):# child class
  def fun_5(self):
    return print("Function 5")

  def fun_6():
    return print("Function 6")

# a1 = a()

# object_b= b

# object_b.fun_1()

object_c = c()

object_c.fun_5()