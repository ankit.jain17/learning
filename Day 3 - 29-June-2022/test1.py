class a:# parent class

  def __init__(self):
    print("constructor of class a")

  def fun_1(self):
   return print("Function 1")

  def fun_2():
    return print("Function 2")

class b(a):# child class
  def __init__(self):
    print("constructor of class b")
  def fun_3():
    return print("Function 3")

  def fun_4():
    return print("Function 4")

objof_b=b()