import pytest


from conf.conftest import input_value


def test_divisible_by_3():
   assert input_value % 3 == 0

def test_divisible_by_6():
   assert input_value % 6 == 0