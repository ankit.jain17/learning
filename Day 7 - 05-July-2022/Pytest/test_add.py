from add import adding


def test_add():

    assert adding(8, 9) == 17

def test_add_string_input():
    result = adding("namste", "world") == "namste world"
    assert result
