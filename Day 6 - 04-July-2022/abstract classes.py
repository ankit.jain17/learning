from abc import ABC, abstractmethod
class computer(ABC):# the class which contains abstract method is known as abstract class
  @abstractmethod
  def process(self):
    # this is a abstract method 
    pass

class laptop(computer):
  def process(self):
    print("abstract method is defined in subclass")

class prog:
  def work(self,com):
    print("sovlve problems")
    com.process()

x = laptop()
x.process()
p = prog()

p.work(x)

