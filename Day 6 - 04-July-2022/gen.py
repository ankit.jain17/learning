def top():
  n=1
  while n<=100:
    sq = n*n
    yield sq
    n += 1

value = top()

for i in value:
  print(i)