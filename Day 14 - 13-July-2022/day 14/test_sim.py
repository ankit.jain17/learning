import pytest

@pytest.fixture(autouse=True, scope="session")

def tc_setup():
    print("Launch Browser")
    print("Login")
    print("Browse Products")
    yield
    print("Log off")
    print("Close Browser")

def test_add_item_to_cart():
    print("Add item successful")

def test_remove_item_from_cart():
    print("Remove Item Successful")

