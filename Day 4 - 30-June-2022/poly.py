class parent:
  def show1(self, fullname, school):
    self.fullname = fullname
    self.school = school
    print(self.fullname, self.school )

# x= parent()
# x.show("name", "school name")

class child(parent):
  def show(self, fullname, school):
    self.fullname = fullname
    self.school = school
    print(self.fullname, self.school )

x= child()
x.show1("boy", "boy name")   
