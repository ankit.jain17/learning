class parent:
  def __init__(self, fullname):
    self.fullname = fullname

  def show(self):
    print("My name is :"+ self.fullname)

class child(parent):
  def __init__(self, fullname, rollno):
    super().__init__(fullname)
    self.rollno = rollno
  
  def show1(self):
    print(self.fullname,self.rollno)

x = parent("a")
x.show()

x = child("angel", 69)
x.show1()