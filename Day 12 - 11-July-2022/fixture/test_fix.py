import pytest


@pytest.fixture(autouse=True)
def variable_x():
    x = 69
    return x


def test_f():
    assert variable_x == 69
